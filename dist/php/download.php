<?php

    $file = $_GET['filename'];

    header("Cache-Control: public");
    header("Content-Description: File Transfer");
    header("Content-Disposition: attachment; filename=".$file);
    header("Content-Transfer-Encoding: binary");
    readfile($file);

    unlink($file);