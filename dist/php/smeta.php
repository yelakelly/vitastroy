<?php
    require 'vendor/autoload.php';

    function cell_range($end_column = '', $first_letters = '') {
        $columns = array();
        $length = strlen($end_column);
        $letters = range('A', 'Z');
    
        // Iterate over 26 letters.
        foreach ($letters as $letter) {
          // Paste the $first_letters before the next.
          $column = $first_letters . $letter; 
          // Add the column to the final array.
          $columns[] = $column;
          // If it was the end column that was added, return the columns.
          if ($column == $end_column)
              return $columns;
        }
    
        // Add the column children.
        foreach ($columns as $column) {
          // Don't itterate if the $end_column was already set in a previous itteration.
          // Stop iterating if you've reached the maximum character length.
          if (!in_array($end_column, $columns) && strlen($column) < $length) {
              $new_columns = myRange($end_column, $column);
              // Merge the new columns which were created with the final columns array.
              $columns = array_merge($columns, $new_columns);
          }
        }
    
        return $columns;
    }

    function y_merge_cells($sheet, $index){
        $sheet->mergeCells(sprintf('A%1$s:B%1$s', $index));
        $sheet->mergeCells(sprintf('C%1$s:F%1$s', $index));
        $sheet->mergeCells(sprintf('H%1$s:I%1$s', $index));
    }

    function showDefaults($sheet, $data){
        $sheet->setCellValue('D11', $data['area'].' м2');
        $sheet->setCellValue('D12', $data['wide'].' м');
        $sheet->setCellValue('D13', $data['length'].' м');
        $sheet->setCellValue('D15', $data['floors']);

        $sheet->setCellValue('D16', $data['mansard'] ? $data['mansard'] : 'Нет');
        $sheet->mergeCells(sprintf('D16:F16', $index));

        $sheet->setCellValue('D17', ($data['length'] * $data['wide'] * 0.5).' м2');
        $sheet->setCellValue('D19', $data['remoteness']);

        $sheet->getStyle('D11:D19')->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
            ]
        ]);

        $sheet->getStyle('D16')->applyFromArray([
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ]
        ]);
    }

    $data = json_decode($_POST['data'], true);
    $headers = $data['headers'];

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

    $inputFileName = 'excel/smeta.xlsx';

    /** Load $inputFileName to a Spreadsheet Object  **/
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
    $spreadsheet->getDefaultStyle()->getFont()->setName('Calibri');
    $spreadsheet->getDefaultStyle()->getFont()->setSize(11);
    
    $sheet = $spreadsheet->getActiveSheet();

    $styleArray = [
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'borders' => [
            'allBorders' => [
                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            ],
        ],
    ];

    showDefaults($sheet, $data);

    $x = 23;
    y_merge_cells($sheet, $x);

    foreach($data['data'] as $key => $item){
       $value = $item[1];
       $title = $item[0];
       $type = $item[3];
       
       if($value) {
            if($key == 'warming'){
                foreach($value as $val){
                    foreach($val['price'] as $v) {
                        y_merge_cells($sheet, $x);
                        $sheet->setCellValue('A'.$x, 'Утепление');
                        $sheet->setCellValue('C'.$x, $val['title']);
                        $sheet->setCellValue('H'.$x, $v[0].'₽');
                        $sheet->setCellValue('G'.$x, $v[1]);

                        $sheet->getStyle(sprintf('A%1$s:I%1$s', $x))->applyFromArray($styleArray);
                        $x+=1;
                    }
                }
            } else if($key == 'windows') {
                foreach($value as $val){
                    if($val[1]){
                        y_merge_cells($sheet, $x);
                        $sheet->setCellValue('A'.$x, 'Окна');
                        $sheet->setCellValue('C'.$x, $val[0]);
                        $sheet->setCellValue('H'.$x, $val[1].'₽');

                        $sheet->getStyle(sprintf('A%1$s:I%1$s', $x))->applyFromArray($styleArray);
                        $x+=1;
                    }
                }
            } else if($key == 'networks') {
                foreach($value as $item){
                    y_merge_cells($sheet, $x);
                    $sheet->setCellValue('A'.$x, 'Инженерные сети');
                    $sheet->setCellValue('C'.$x, $item[0]);
                    $sheet->setCellValue('H'.$x, $item[1].'₽');

                    $sheet->getStyle(sprintf('A%1$s:I%1$s', $x))->applyFromArray($styleArray);
                    $x+=1;
                }
            } else {
                y_merge_cells($sheet, $x);
                $sheet->setCellValue('A'.$x, $title);
                $sheet->setCellValue('C'.$x, $value);
                $sheet->setCellValue('H'.$x, $item[2].'₽');
                $sheet->setCellValue('G'.$x, $type);
                
                $sheet->getStyle(sprintf('A%1$s:I%1$s', $x))->applyFromArray($styleArray);
                $x += 1;
            }
       }
    }

    $sheet->mergeCells(sprintf('A%1$s:G%1$s', $x));
    $sheet->setCellValue('A'.$x, 'Итого');
    
    $sheet->mergeCells(sprintf('H%1$s:I%1$s', $x));
    $sheet->setCellValue('H'.$x, $data['totalPrice'].'₽');
    $sheet->getStyle(sprintf('A%1$s:I%1$s', $x))->applyFromArray($styleArray);
    $sheet->getStyle('A'.$x)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);

    $x += 2;

    $sheet->mergeCells(sprintf('A%1$s:I%1$s', $x));
    $sheet->setCellValue('A'.$x, 'Проект Вашего дома мы сделаем Вам в подарок при обращение в течение 5 дней!');
    $sheet->getStyle('A'.$x)->applyFromArray([
        'alignment' => [
            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
        ],
        'font' => [
            'bold' => true,
        ],
    ]);

    $x += 4;

    $sheet->mergeCells(sprintf('A%1$s:G%1$s', $x));
    $sheet->setCellValue('A'.$x, 'Предложение не является публичной офертой');
    $sheet->getStyle('A'.$x)->applyFromArray([
        'font' => [
            'italic' => true,
        ],
    ]);


    $filename = 'smeta'.uniqid().'.xlsx';
    $writer = new Xlsx($spreadsheet);
    $writer->save($filename);
    echo $filename;