'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/** Класс представляет этап в калькуляторе. */
var Stage = function () {
    /**
     * @param  {object} settings
     */
    function Stage(settings) {
        _classCallCheck(this, Stage);

        this.el = settings['el'];
        this.title = settings['title'];
        this.name = settings['name'];
        this.nextStep = settings['nextStep'];
        this.prevStep = settings['prevStep'];
        this.onActive = settings['onActive'];
        this.onDisable = settings['onDisable'];
        this.compability = settings['compability'];
    }

    /** Инициализация */


    _createClass(Stage, [{
        key: 'init',
        value: function init() {
            if (!this.initialize) {
                this.setEvents();
            }

            this.initialize = true;
        }

        /** Установка событий */

    }, {
        key: 'setEvents',
        value: function setEvents() {}

        /** Очищение состояния */

    }, {
        key: 'setClear',
        value: function setClear() {
            var val = appState.getValue(this.name);

            if (Array.isArray(val)) {
                appState.setValue(this.name, []);
            } else if (typeof val == 'string') {
                appState.setValue(this.name, '');
            } else if (!isNaN(val)) {
                val = 0;
            }

            $(this.el).find('.panel.is-active').removeClass('is-active');
        }

        /** Отключает этап */

    }, {
        key: 'setDisabled',
        value: function setDisabled() {
            $(this.el).find('.is-disable').removeClass('is-disable');
            $(this.el).addClass('not-active');
        }

        /** Проверяет совместимость **/

    }, {
        key: 'checkCompability',
        value: function checkCompability() {
            // Получаем текущие материалы
            // Проверяем совместимость и выключаем несовместимые.
            var base = appState.getValue('base');
            var currentCols = $(this.el).find('.panel.is-selectable');
            currentCols.removeClass('is-disable');

            if (base) {
                // Получаем базовые значение

                var rules = [base];
                var floorsCount = appState.getIndex('floors');

                var bannedMaterials = [];

                var disabledButtons = currentCols.filter(function () {
                    var isActive = void 0;
                    var material = $(this).attr('material') || $(this).attr('base');

                    if (material.startsWith('ф')) {
                        rules = [].concat(_toConsumableArray(appState.getValue('innerWalls')), _toConsumableArray(appState.getValue('outerWalls')));
                        isActive = Compability.getCompabilityByBase(material, rules, floorsCount);
                    } else {
                        isActive = Compability.getCompability(material, rules, floorsCount);
                    }

                    if (!isActive) {
                        bannedMaterials.push(material);
                    }

                    return !isActive;
                });

                if (bannedMaterials.length) appState.clearByName(this.name, bannedMaterials);

                disabledButtons.removeClass('is-active').addClass('is-disable');
            } else if (this.name == 'base') {
                var floors = +$('#floors').val();

                if (floors > 1) {
                    $('[base="ф0001"]').addClass('is-disable');
                }
            }
        }

        /** Делает этап активным */

    }, {
        key: 'setActive',
        value: function setActive() {
            if (this !== mansardStage && this.compability !== false) {
                this.checkCompability();
            }

            $(this.el).addClass('is-active');
            $(this.el).removeClass('not-active');
        }

        /** Переход к следующим этапам */

    }, {
        key: 'goToNextStep',
        value: function goToNextStep() {
            var nextStep = this.nextStep;
            var currentStage = steps.getCurrentStage();

            if (nextStep && nextStep !== currentStage) {
                var prevStage = steps.getPrevStage();

                if (prevStage) {
                    prevStage.setDisabled();
                }

                if (Array.isArray(nextStep)) {
                    nextStep.forEach(function (stage) {
                        return steps.addNewStage(stage);
                    });
                } else {
                    steps.addNewStage(nextStep);
                }
            }

            if (currentStage.onActive) {
                currentStage.onActive();
            }
        }

        /** Делает переход к предыдущему этапу */

    }, {
        key: 'goToPrevStep',
        value: function goToPrevStep() {
            var currentStage = steps.getCurrentStage();
            var prevStep = currentStage.prevStage;

            if (prevStep !== false) {
                steps.setPrevStage(prevStep);
            }

            if (currentStage.onDisable) {
                currentStage.onDisable();
            }
        }
    }]);

    return Stage;
}();

var steps = function () {
    var stages = [];

    /**
      * Функция получает этап по имени
      * @param {string} name - имя этапа
      * @return {object} Stage instance
    **/
    function getStageByName(name) {
        for (var i = 0; i < stages.length; i++) {
            if (stages[i].name == name) {
                return stages[i];
            }
        }
    }

    /**
      * Функция устанавливает активным предыдущий этап
    **/
    function setPrevStage() {
        var currentStage = getCurrentStage();
        currentStage.setDisabled();
        removeLastStage();

        var prevStage = getPrevStage();
        prevStage.setActive();
    }

    /**
      * Удаляет последний этап
    **/
    function removeLastStage() {
        stages.splice(-1, 1);
    }

    /**
      * Функция добавляет новый этап и активирует его.
      * @param {Stage} stage
    **/
    function addNewStage(stage) {
        stage.init();
        stages.push(stage);

        if (stage.onActive) {
            stage.onActive();
        }

        stage.setActive();
    }

    /**
      * Функция возвращает текущий этап
      * @return {Stage} Stage instance
    **/
    var getCurrentStage = function getCurrentStage() {
        return stages[stages.length - 1];
    };

    var getPrevStage = function getPrevStage() {
        return stages[stages.length - 1 - 1] || false;
    };

    /**
      * Функция возвращает все этапы
      * @return {array} Массив всех этапов
    **/
    var getStages = function getStages() {
        return stages;
    };

    /**
     * Удаляет шаг
     * @param  {Stage} stage
     */
    function removeStage(stage) {
        stages.forEach(function (st, index) {
            if (st == stage) {
                stages.splice(index, 1);
            }
        });
    }

    return {
        'addNewStage': addNewStage,
        'setPrevStage': setPrevStage,
        'getPrevStage': getPrevStage,
        'getCurrentStage': getCurrentStage,
        'getStageByName': getStageByName,
        'getStages': getStages,
        'removeStage': removeStage
    };
}();

var networksStage = new Stage({
    'el': $('.calculator-section--networks'),
    'title': 'Инженерные сети',
    'name': 'networks',
    'prevStep': null
});

var facadeStage = new Stage({
    'el': $('.calculator-section--facade'),
    'title': 'Фасад',
    'name': 'facade',
    'prevStep': null
});

var warmingStage = new Stage({
    'el': $('.calculator-section--warming'),
    'title': 'Утепление',
    'name': 'warming',
    'prevStep': null
});

var otherStage = new Stage({
    'el': $('.calculator-section--windows'),
    'title': 'Окна',
    'name': 'windows',
    'prevStep': null
});

var sidingStage = new Stage({
    'el': $('.calculator-section--siding'),
    'title': 'Сайдинг',
    'name': 'siding',
    'prevStep': null,
    'compability': false
});

var roofStage = new Stage({
    'el': $('.calculator-section--roof'),
    'title': 'Кровля',
    'name': 'roof',
    'prevStep': false
});

var mansardStage = new Stage({
    'el': $('.calculator-section--mansard'),
    'title': 'Мансарда',
    'name': 'mansard',
    'nextStep': roofStage,
    'compability': false
});

mansardStage.goToNextStep = function () {
    var mansardMaterial = appState.getIndex('mansardMaterial');

    if (mansardMaterial !== 'м0047') {
        this.__proto__.goToNextStep.call(this);
    } else {
        roofStage.setClear();
        var currentStage = steps.getCurrentStage();

        if (currentStage !== this) {
            steps.setPrevStage();
        }
    }
};

mansardStage.onActive = function () {
    if ($(this.el).find('.panel').length == 2) {
        return;
    }

    var wallMaterial = appState.getValue('outerWalls');
    var value = wallMaterial[wallMaterial.length - 1];
    var container = $(this.el).find('.calculator-section__container');

    $(renderPanel({
        'type': 'material',
        'value': value,
        'event': 'setMansardMaterial',
        'state': 'mansard',
        'title': materialsInfo[value].title,
        'img': materialsInfo[value].img
    })).appendTo(container);

    var column = container.find('.panel').last();
    App.setState.call(column);
};

mansardStage.onDisable = function () {
    var panels = $(this.el).find('.calculator-section__col');
    if (panels.length > 1) {
        panels.last().remove();
    }
};

var overlappingStage = new Stage({
    'el': $('.calculator-section--overlapping'),
    'title': 'Перекрытия',
    'name': 'overlapping',
    'nextStep': roofStage
});

overlappingStage.checkCompability = function () {
    var currentFloor = $(this.el).find('.calculator-section__floor-control.is-active').attr('floor');
    currentFloor = !isNaN(currentFloor) ? currentFloor - 1 : 1;
    var floorsCount = appState.getIndex('floors');

    var inWalls = void 0;
    var exWalls = void 0;

    if (currentFloor) {
        inWalls = appState.getValue('innerWalls')[currentFloor - 1];
        exWalls = appState.getValue('outerWalls')[currentFloor - 1];
    } else {
        inWalls = appState.getValue('innerWalls')[currentFloor];
        exWalls = appState.getValue('outerWalls')[currentFloor];
    }

    var _bases = [];

    var currentCols = $(this.el).find('.panel.is-selectable');
    currentCols.removeClass('is-disable');

    if (inWalls) _bases.push(inWalls);
    if (exWalls) _bases.push(exWalls);

    var disabledButtons = currentCols.filter(function () {
        var isActive = void 0;
        var material = $(this).attr('material') || $(this).attr('base');

        isActive = Compability.getCompabilityByBase(material, _bases, floorsCount, currentFloor + 1);

        return !isActive;
    });

    disabledButtons.removeClass('is-active').addClass('is-disable');
};

var innerWalls = new Stage({
    'el': $('.calculator-section--inner-walls'),
    'title': 'Внутренние стены',
    'name': 'innerWalls',
    'nextStep': overlappingStage
});

var outerWalls = new Stage({
    'el': $('.calculator-section--external-walls'),
    'title': 'Внешние стены',
    'name': 'outerWalls',
    'nextStep': innerWalls
});

var foundationStage = new Stage({
    'el': $('.calculator-section--foundation'),
    'title': 'Тип фундамента',
    'name': 'base',
    'nextStep': outerWalls,
    'onActive': function onActive() {
        var activeSteps = [networksStage, facadeStage, warmingStage, otherStage, sidingStage];

        activeSteps.forEach(function (step) {
            step.setActive();
        });
    },
    'onDisable': function onDisable() {
        var activeSteps = [networksStage, facadeStage, warmingStage, otherStage, sidingStage];

        activeSteps.forEach(function (step) {
            step.setDisabled();
        });
    }
});

foundationStage.checkCompability = function () {
    this.__proto__.checkCompability.call(this);

    [roofStage, facadeStage].forEach(function (step) {
        step.checkCompability();
    });
};

var indexStage = new Stage({
    'el': $('.calculator-section--dimensions'),
    'title': 'Параметры строения',
    'name': 'dimensions',
    'nextStep': foundationStage
});

indexStage.setEvents = function () {
    var self = this;
    var el = $(this.el);

    // TODO: сделать более универсально

    $('#floors').on("keyup change", function () {
        var value = this.value;

        if (value == '' || isNaN(value)) {
            return;
        }

        value = +value;

        if (value > 3) {
            value = 3;
            this.value = 3;
        }

        if (value <= 0) {
            value = 1;
            this.value = 1;
        }

        if (value > 1) {
            $('.has-floors .calculator-section__controls').html(renderFloors(value));
        } else {
            // fix нуллей в стенах
            ['innerWalls', 'outerWalls', 'overlapping'].forEach(function (key) {
                return appState.setValue(key, []);
            });

            $(outerWalls.el, innerWalls.el, overlappingStage.el).find('.panel.is-active').removeClass('is-active');

            $('.has-floors .calculator-section__controls').html('');
        }

        value > 1 ? $('[base="ф0001"]').addClass('is-disable') : $('[base="ф0001"]').removeClass('is-disable');
    });

    // Выводим площадь при изменении базовых значений

    el.find('#wide, #length, #floors, #walls, #remoteness').each(function () {
        $(this).on('keyup change', function () {

            el.find('input[name="walls"]').val(self.calculateInnerWalls);
            el.find('.calculator-section__area-total > span').last().html(self.calculateArea() + '<span>\u043C<sup>2</sup></span>');
        });
        App.calculatePrice();
    });

    // При заполнении всех полей, делаем доступным блок Фундамент

    // Вынести в метод

    $('#wide, #length, #floors').on('keyup change', function () {
        var requiredCount = 0;

        $('#wide, #length, #floors').each(function () {
            $(this).val() ? requiredCount++ : null;
        });

        var currentStage = steps.getCurrentStage();

        if (requiredCount == 3 && currentStage !== foundationStage) {
            self.goToNextStep();
        }

        if (currentStage == foundationStage && requiredCount < 3) {
            self.goToPrevStep();
        }
    });

    // Блок мансарда

    el.find('#mansard').on('change', function () {
        var name = $(this).attr('name');
        var value = $(this).is(':checked') ? true : false;

        appState.setIndex(name, !!$(this).val());

        if (value) {
            $('.calculator-section--mansard').removeClass('is-hidden');
            overlappingStage.nextStep = mansardStage;
        } else {
            $('.calculator-section--mansard').addClass('is-hidden');
            overlappingStage.nextStep = roofStage;
        }

        calculateSteps();
    });
};

indexStage.calculateArea = function () {
    return +$('#wide').val() * +$('#length').val() * +$('#floors').val() || 0;
};
indexStage.calculateInnerWalls = function () {
    return +$('#wide').val() * +$('#length').val() * 0.5 || 0;
};

steps.addNewStage(indexStage);

App.init();