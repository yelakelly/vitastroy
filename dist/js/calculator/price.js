'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// if the module has no dependencies, the above pattern can be simplified to
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if ((typeof module === 'undefined' ? 'undefined' : _typeof(module)) === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.Prices = factory();
    }
})(typeof self !== 'undefined' ? self : undefined, function () {
    var Prices = {
        'ф0001': {
            'title': 'Ленточный мелкозаглублённый',
            'price': [3097.9411760000003, null]
        },
        'ф0002': {
            'title': 'Ленточный заглублённый',
            'price': [4799.830376, null]
        },
        'ф0003': {
            'title': 'Ростверк на буронабивных сваях',
            'price': [4155.262376, null]
        },
        'ф0004': {
            'title': 'Ленточный заглублённый с монолитным цоколем',
            'price': [4900.508376, null]
        },
        'ф0005': {
            'title': 'Цокольный этаж [подвал]',
            'price': [12952.715, null]
        },
        'с0006': {
            'title': 'Кирпич',
            'type': '250',
            'price': [5485.972, 4114.478999999999]
        },
        'с0007': {
            'title': 'Кирпич',
            'type': '380',
            'price': [4723.87, 3542.9025]
        },
        'с0008': {
            'title': 'Каркасные по системе сэндвич[сип]',
            'price': [3653.932, 2740.4489999999996]
        },
        'с0009': {
            'title': 'Блок газоселикатный',
            'price': [2157.1092, 1617.8319]
        },
        'с0010': {
            'title': 'Монолитный железобетон',
            'price': [4916.7224, 3687.5418]
        },
        'с0011': {
            'title': 'Керамзито - бетонный блок',
            'price': [2869.374, 2152.0305]
        },
        'п0031': {
            'title': 'Деревянные',
            'price': [850.1905999999999, null]
        },
        'п0032': {
            'title': 'Сборный железобетон',
            'price': [3707.071, null]
        },
        'п0033': {
            'title': 'Монолитный железобетон',
            'price': [2576.35, null]
        },
        'к0047': {
            'title': 'Гибкая черепица',
            'price': [7180.8794, 7180.8794]
        },
        'к0048': {
            'title': 'Металлочерепица',
            'price': [5184.209000000001, 5184.209000000001]
        },
        'к0050': {
            'title': 'Керамическая черепица',
            'price': [12528.859099999998, 12528.859099999998]
        },
        'к0052': {
            'title': 'Ондулин',
            'price': [4962.7970000000005, 4962.7970000000005]
        },
        'к0053': {
            'title': 'Профлист',
            'price': [4761.937000000001, 4761.937000000001]
        },
        'с0014': {
            'title': 'Кирпич',
            'price': [1224.4140000000002, 918.3105000000002]
        },
        'с0016': {
            'title': 'Каркасные по системе сэндвич [сип]',
            'price': [3653.932, 2740.4489999999996]
        },
        'с0017': {
            'title': 'Блок газоселикатный',
            'price': [497.63599999999997, 373.227]
        },
        'с0018': {
            'title': 'Монолитные железобетонные',
            'price': [4916.7224, 3687.5418]
        },
        'с0019': {
            'title': 'Керамзито-бетонный блок',
            'price': [712.5609999999999, 534.42075]
        },
        'и0054': {
            'title': 'Инж сети электрика',
            'price': [1600, 1400]
        },
        'и0055': {
            'title': 'Инж сети отопление, водоснабж. и канализац.',
            'price': [3500, 3800]
        },
        'у0022': {
            'title': 'Пенополистерол',
            'type': 50,
            'price': [394.603, 295.95225]
        },
        'у0023': {
            'title': 'Пенополистерол',
            'type': 100,
            'price': [518.155, 388.61625]
        },
        'у0024': {
            'title': 'Пенополистерол',
            'type': 150,
            'price': [590.095, 442.57125]
        },
        'у0025': {
            'title': 'Экструдированный пенополистерол',
            'type': '50 мм',
            'price': [564.311, 423.23325]
        },
        'у0026': {
            'title': 'Экструдированный пенополистерол',
            'type': '100 мм',
            'price': [875.105, 656.32875]
        },
        'у0027': {
            'title': 'Экструдированный пенополистерол',
            'type': '150 мм',
            'price': [1105.6950000000002, 829.2712500000001]
        },
        'у0028': {
            'title': 'Минеральная вата',
            'type': '50 мм',
            'price': [553.195, 414.89625]
        },
        'у0029': {
            'title': 'Минеральная вата',
            'type': '100 мм',
            'price': [832.4275, 624.3206250000001]
        },
        'у0030': {
            'title': 'Минеральная вата',
            'type': '150 мм',
            'price': [1002.695, 752.02125]
        },
        'о0035': {
            'title': 'Облицовочный кирпич',
            'price': [2140.7349999999997, 1605.5512499999998]
        },
        'о0036': {
            'title': 'Декоративная штукатурка',
            'price': [1208.6926, 906.51945]
        },
        'о0037': {
            'title': 'Сайдинг',
            'price': [1870.6203, 1402.965225]
        },
        'о0056': {
            'title': 'Эконом',
            'price': [4000, 3000]
        },
        'о0057': {
            'title': 'Стандарт',
            'price': [8000, 6000]
        },
        'о0058': {
            'title': 'Премиум',
            'price': [12000, 9000]
        },
        'о0000': {
            'title': 'Без отделки',
            'price': [0, 0]
        },
        'дов0038': {
            'title': 'Двери стальные >= 1,5',
            'price': [5000, 5000]
        },
        'дов0039': {
            'title': 'Двери стальные <= 1,5',
            'price': [1000, 1000]
        },
        'дов0040': {
            'title': 'Двери дизайнерские',
            'price': [30000, 30000]
        },
        'дов0043': {
            'title': 'Эконом',
            'price': [3000, 3000]
        },
        'дов0044': {
            'title': 'Стандарт',
            'price': [5000, 5000]
        },
        'дов0045': {
            'title': 'Премиум',
            'price': [10000, 10000]
        },
        'дов0046': {
            'title': 'Ворота',
            'price': [1000, 1000]
        },
        'м0047': {
            'title': 'Мансардный этаж из деревянных конструкций',
            'price': [null, 7885.2762]
        }
    };

    return Prices;
});