'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// if the module has no dependencies, the above pattern can be simplified to
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if ((typeof module === 'undefined' ? 'undefined' : _typeof(module)) === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.areaDependency = factory();
    }
})(typeof self !== 'undefined' ? self : undefined, function () {
    var vals = [[70, 1.3], [90, 1.23], [110, 1.15], [130, 1.075], [150, 1], [170, 1.0], [190, 0.99], [210, 0.99], [230, 0.99], [250, 0.98], [270, 0.98], [290, 0.98], [310, 0.97], [330, 0.97], [350, 0.97], [370, 0.96], [390, 0.96], [410, 0.96], [430, 0.95], [450, 0.95], [470, 0.95], [490, 0.95], [510, 0.94], [530, 0.94], [550, 0.94], [570, 0.93], [590, 0.93], [610, 0.93], [630, 0.92], [650, 0.92], [670, 0.92], [690, 0.91], [710, 0.91], [730, 0.91], [750, 0.9], [770, 0.9], [790, 0.9], [810, 0.89], [830, 0.89], [850, 0.89], [870, 0.88], [890, 0.88], [910, 0.88], [930, 0.87], [950, 0.87], [970, 0.87], [990, 0.86], [1010, 0.86], [1030, 0.86], [1050, 0.85], [1070, 0.85], [1090, 0.85], [1110, 0.85], [1130, 0.84], [1150, 0.84], [1170, 0.84], [1190, 0.83], [1210, 0.83], [1230, 0.83], [1250, 0.82], [1270, 0.82], [1290, 0.82], [1310, 0.81], [1330, 0.81], [1350, 0.81], [1370, 0.8], [1390, 0.8], [1410, 0.8], [1430, 0.79], [1450, 0.79], [1470, 0.79], [1490, 0.78], [1510, 0.78], [1530, 0.78], [1550, 0.77], [1570, 0.77], [1590, 0.77], [1610, 0.76], [1630, 0.76], [1650, 0.76], [1670, 0.75], [1690, 0.75], [1710, 0.75], [1730, 0.75], [1750, 0.74], [1770, 0.74], [1790, 0.74], [1810, 0.73], [1830, 0.73], [1850, 0.73], [1870, 0.72], [1890, 0.72], [1910, 0.72], [1930, 0.71], [1950, 0.71], [1970, 0.71], [1990, 0.7], [2010, 0.7]];

    /**
     * Получение коэффициента
     * @param {integer} area
     * @return {integer}
     */
    function getAreaCoefficient(area) {
        for (var i = 1; i < vals.length - 1; i++) {
            var curr = vals[i];
            var next = vals[i + 1];

            if (area >= curr[0] && area <= next[0]) {
                return next[1];
            }
        }
        return 1.3;
    }

    return {
        'getCompability': getAreaCoefficient
    };
});