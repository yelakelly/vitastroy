'use strict';

/**
 * @module Utils
 */

if (!Object.entries) {
  Object.entries = function (obj) {
    var ownProps = Object.keys(obj);
    var i = ownProps.length;
    var resArray = new Array(i);

    while (i--) {
      resArray[i] = [ownProps[i], obj[ownProps[i]]];
    }

    return resArray;
  };
}

var materialsInfo = {
  'с0006': {
    'title': 'Кирпич 250',
    'img': 'kirpich-250.png'
  },
  'с0007': {
    'title': 'Кирпич 380',
    'img': 'kirpich-380.png'
  },
  'с0008': {
    'title': 'Каркас по <br> системе сендвич',
    'img': 'karkasnie-po-sisteme-sendvich.png'
  },
  'с0009': {
    'title': 'Блок газоселикатный',
    'img': 'penoblock.png'
  },
  'с0010': {
    'title': 'Монолит <br> железобетонный',
    'img': 'monolitnie-zelezobetonnie.png'
  },
  'с0011': {
    'title': 'Керамзито-<br>бетонный блок',
    'img': 'keramzitno-betonny-block.png'
  }
};

/**
* Рендер кнопок этажей
* @param  {object} settings Настройки
* @return {string}
*/
function renderPanel(settings) {
  var html = '<div class="calculator-section__col">\n    <div class="panel is-selectable" event="' + settings.event + '" state="' + settings.state + '" \n    event-type="' + (settings.eventType || 'click') + '" \n    ' + (settings.type || 'material') + '="' + settings.value + '">\n      <img class="panel__thumb" src="img/' + settings.img + '">\n      <p class="panel__header"><span>' + settings.title + '</span></p>\n    </div>\n  </div>';

  return html;
}

Handlebars.registerHelper('floor', function (val) {
  val = Handlebars.Utils.escapeExpression(val);

  return new Handlebars.SafeString(Math.floor(val));
});

Handlebars.registerHelper('prettyNumber', function (val) {
  val = Handlebars.Utils.escapeExpression(val);
  return new Handlebars.SafeString(numberWithSpaces(val));
});

function numberWithSpaces(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

function renderTotalPrice(price) {
  price = numberWithSpaces(Math.ceil(price));

  $('.calculator-section__total-price').html(price + ' \u0440\u0443\u0431.');
}

/**
 * Рендер кнопок этажей
 * @param  {integer} floorsCount Количество этажей
 * @return {string}
 */
function renderFloors(floorsCount) {
  var html = '';

  for (var i = 1; i < floorsCount + 1; i++) {
    html += '<span class="calculator-section__floor-control" floor="' + i + '">' + i + '\u0439 \u044D\u0442\u0430\u0436: </span>';
  }

  // Делаем 1 этаж активным

  html = $(html);
  html.eq(0).addClass('is-active');

  return html;
}

/**
 * Перасчет шагов калькулятора
 */
function calculateSteps() {
  $('.calculator-section').not('.is-hidden').each(function (index) {
    $(this).find('.calculator-section__header span:first-child').html(index + 1);
  });
}

Handlebars.registerHelper({
  eq: function eq(v1, v2) {
    return v1 === v2;
  },
  ne: function ne(v1, v2) {
    return v1 !== v2;
  },
  lt: function lt(v1, v2) {
    return v1 < v2;
  },
  gt: function gt(v1, v2) {
    return v1 > v2;
  },
  lte: function lte(v1, v2) {
    return v1 <= v2;
  },
  gte: function gte(v1, v2) {
    return v1 >= v2;
  },
  and: function and(v1, v2) {
    return v1 && v2;
  },
  or: function or(v1, v2) {
    return v1 || v2;
  }
});

// const LOGGER = true;

// let logger = (function(){
//   let logs = [];

//   if (LOGGER) {
//     logs.forEach(function(message){
//       console.log(message);
//     })
//   };

//   function addLogMessage(message) {
//     logs.push(message);
//   }

//   return {
//     'addLogMessage': addLogMessage
//   }
// })();