'use strict';

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var appState = function () {
    var config = {
        'index': {
            'wide': 0,
            'length': 0,
            'floors': 0,
            'walls': 0,
            'mansard': false,
            'mansardMaterial': '',
            'remoteness': 0
        },
        'base': '',
        'outerWalls': [],
        'innerWalls': [],
        'overlapping': [],
        'roof': '',
        'networks': [],
        'facade': '',
        'warming': {
            'warming_1': [],
            'warming_2': [],
            'warming_3': []
        },
        'windows': {
            'windows_1': 0,
            'windows_2': 0,
            'windows_3': 0
        },
        'doors': '',
        'gate': ''
    };
    /**
     * @param  {string} key
     * @param {array} values
     */
    function clearByName(key, values) {
        var configValue = config[key];

        values.forEach(function (val) {
            if (Array.isArray(configValue)) {
                var clean = configValue.filter(function (el, index) {
                    return el !== val;
                });

                config[key] = clean;
            } else if (typeof val == 'string') {
                if (val == configValue) {
                    config[key] = '';
                }
            }
        });
    }

    /**
     * Устанавливает новое значение
     * @param  {string} key
     * @param  {string} value
     */
    function set(key, value) {
        config[key] = value;
    }

    /**
     * Возвращает значение параметра.
     *
     * @param  {string} key
     * @return {string}
     */
    var get = function get(key) {
        return config[key];
    };

    /**
     * Получает базовое значение.
     *
     * @param  {string} key
     * @return {array}
     */
    var getWarming = function getWarming(key) {
        return config['warming'][key];
    };

    /**
     * Устанавливает значение утепления.
     *
     * @param  {string} key
     * @param {array} value
     */
    function setWarming(key, value) {
        config['warming'][key] = value;
    }

    /**
     * Получает базовое значение.
     *
     * @param  {string} key
     * @return {string}
     */
    var getWindow = function getWindow(key) {
        return config['windows'][key];
    };

    /**
     * Устанавливает значение окон.
     *
     * @param  {string} key
     * @param {string} value
     */
    function setWindow(key, value) {
        config['windows'][key] = value;
    }

    /**
     * Получает базовое значение.
     *
     * @param  {string} key
     * @return {string}
     */
    var getIndex = function getIndex(key) {
        return config['index'][key];
    };

    /**
     * Устанавливает базовое значение.
     *
     * @param  {string} key
     * @param {string} value
     */
    function setIndex(key, value) {
        config['index'][key] = value;
    }

    /**
     * Возвращает значение параметра, у которого
     * есть этажи.
     *
     * @param  {string} key
     * @param  {string} floor
     * @return {string}
     */
    function getWithFloor(key, floor) {
        if (config[key][floor]) {
            return config[key][floor];
        } else {
            return false;
        }
    }

    /**
     * Устанавливает значение параметра, у которого
     * есть этажи.
     *
     * @param  {string} key
     * @param  {integer} value
     * @param  {integer} floor
     */
    function setWithFloor(key, value, floor) {
        if (config[key][floor] && config[key][floor] == value) {
            config[key][floor] = null;
        } else {
            config[key][floor] = value;
        }
    }

    /**
     * Возвращает объект стейта.
     * @return {object}
     */
    var getAll = function getAll() {
        return config;
    };

    return {
        'setValue': set,
        'getValue': get,
        'setIndex': setIndex,
        'getIndex': getIndex,
        'getAll': getAll,
        'setWithFloor': setWithFloor,
        'getWithFloor': getWithFloor,
        'setWarming': setWarming,
        'getWarming': getWarming,
        'setWindow': setWindow,
        'getWindow': getWindow,
        'clearByName': clearByName
    };
}();

// if the module has no dependencies, the above pattern can be simplified to
(function (root, factory, appState) {
    if (typeof define === 'function' && define.amd) {
        define('App', [], factory);
        define('appState', [], appState);
    } else if ((typeof module === 'undefined' ? 'undefined' : _typeof(module)) === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports.App = factory(require('./area-dependency'), require('./price'));
        module.exports.appState = appState;
    } else {
        // Browser globals (root is window)
        root.appState = appState;
        root.App = factory(root.areaDependency, root.Prices);
    }
})(typeof self !== 'undefined' ? self : undefined, function (areaDependency, Prices) {
    var calcState = {
        'base': ['Фундамент'],
        'outerWalls': ['Стены наружные'],
        'innerWalls': ['Стены внутренние'],
        'overlapping': ['Перекрытия'],
        'roof': ['Кровля'],
        'networks': ['Инж сети', []],
        'facade': ['Фасад'],
        'warming': ['Утепление', {
            'warming_1': { 'title': 'Пенополистерол', 'price': [] },
            'warming_2': { 'title': 'Экструдированный пенополистерол', 'price': [] },
            'warming_3': { 'title': 'Минеральная вата', 'price': [] }
        }],
        'windows': ['Двери, окна, ворота', {
            'windows_1': ['Эконом', 0],
            'windows_2': ['Стандарт', 0],
            'windows_3': ['Премиум', 0]
        }],
        'doors': ['Двери, окна, ворота', []],
        'gate': ['Двери, окна, ворота'],
        'siding': ['Отделка'],
        'mansard': ['Мансардный этаж']
    };

    var defaultEvent = function defaultEvent(attrs) {
        if ($(this).hasClass('is-disable')) return;

        var currentValue = appState.getValue(attrs.state);
        var newValue = attrs.val;
        var step = steps.getStageByName(attrs.state);

        if (Array.isArray(currentValue)) {
            var index = currentValue.indexOf(newValue);

            if (!~index) {
                currentValue.push(newValue);
            } else {
                currentValue.splice(index, 1);
            }

            appState.setValue(attrs.state, currentValue);
        } else {
            if (currentValue == newValue) {
                appState.setValue(attrs.state, null);
            } else {
                appState.setValue(attrs.state, newValue);
            }
        }

        if (step) {
            if (step.nextStep && step.nextStep !== mansardStage) {
                step.nextStep.checkCompability();
            }

            if (currentValue !== newValue) {
                step.goToNextStep();
            } else {
                step.goToPrevStep();
                step.checkCompability();
            }
        }

        calculatePrice();
    };

    var disableWarmingChoices = function disableWarmingChoices(el) {
        // Получить родителя, если есть занятые, то отключить 
        var section = el.closest('.calculator-section__col');
        var checkedCount = section.find('input[type="checkbox"]').filter(':checked').length;

        if (checkedCount) {
            section.siblings().addClass('is-disabled');
        } else {
            section.siblings().removeClass('is-disabled');
        }
    };

    var customEvents = {
        'setWithFloors': function setWithFloors(attrs) {
            if ($(this).hasClass('is-disable')) return;

            var section = $(this).closest('.calculator-section');
            var floor = section.find('.calculator-section__floor-control.is-active').attr('floor');

            floor = isNaN(floor) ? 1 : +floor;

            var floorsCount = appState.getIndex('floors');
            appState.setWithFloor(attrs.state, attrs.val, floor - 1);

            var values = appState.getValue(attrs.state);
            var notEmptyValues = values.filter(function (val) {
                return !!val;
            });
            var currentStage = steps.getStageByName(attrs.state);

            calculatePrice();

            if (currentStage) {
                if (currentStage.nextStep !== mansardStage) {
                    currentStage.nextStep.checkCompability();
                }

                if (floorsCount == notEmptyValues.length && currentStage == steps.getCurrentStage()) {
                    currentStage.goToNextStep();
                    return;
                }

                /* TODO: fix */

                if ((floorsCount !== notEmptyValues.length || !floorsCount) && currentStage !== steps.getCurrentStage()) {
                    currentStage.goToPrevStep();
                }
            }
        },
        'setIndex': function setIndex(attrs) {
            if ($(this).hasClass('is-disable')) return;

            var currentStage = steps.getStageByName(attrs.state);
            appState.setIndex(attrs.state, +this.value);

            calculatePrice();

            if (currentStage && currentStage.nextStep && currentStage.nextStep !== mansardStage) {
                currentStage.nextStep.checkCompability();
            }
        },
        'setWindows': function setWindows(attrs) {
            if ($(this).hasClass('is-disable')) return;
            var val = +$(this).val();

            if (val) {
                appState.setWindow(attrs.state, val);
            } else {
                appState.setWindow(attrs.state, 0);
            }

            calculatePrice();
        },
        'setWarming': function setWarming(attrs) {
            if ($(this).hasClass('is-disable')) return;
            disableWarmingChoices($(this));

            var currentValues = appState.getWarming(attrs.state);
            var isExist = currentValues.indexOf(attrs.val);

            if (~isExist) {
                currentValues.splice(isExist, 1);
            } else {
                currentValues.push(attrs.val);
            }

            appState.setWarming(attrs.state, currentValues);
            calculatePrice();
        },
        'setMansardMaterial': function setMansardMaterial(attrs) {
            if ($(this).hasClass('is-disable')) return;
            var currentValue = appState.getIndex('mansardMaterial');
            var step = steps.getStageByName('mansard');

            appState.setIndex('mansardMaterial', attrs.val);

            if (step) {
                if (currentValue !== attrs.val) {
                    step.goToNextStep();
                } else {
                    appState.setIndex('mansardMaterial', null);
                    step.goToPrevStep();
                }
            }

            calculatePrice();
        }
    };

    /**
     * Ввод только чисел
     */
    function validateNumbers() {
        var val = $(this).val();

        // Запрещаем вводить не числа

        if (isNaN(val) && $(this).attr('type') !== 'checkbox') {
            $(this).val(val.replace(/\D/g, ''));
        }
    }

    /**
     * Устанавливает события
     */
    function setState() {
        var attrs = {
            'state': $(this).attr('state'),
            'eventType': $(this).attr('event-type'),
            'event': $(this).attr('event'),
            'val': $(this).attr('base') || $(this).attr('material')
        };

        $(this).on(attrs.eventType, function () {
            validateNumbers.call(this);

            if (attrs.event) {
                customEvents[attrs.event].call(this, attrs);
            } else {
                defaultEvent.call(this, attrs);
            }
        });
    }

    /**
     * Вкл/выкл кнопки калькулятора
     * @param  {event} e
     */
    function toggleButtons(e) {
        var currentState = $(this).attr('state');
        var activeElements = $('body').find('[state="' + currentState + '"].is-active');

        if ($(this).hasClass('is-disable')) return;

        if ($(this).hasClass('is-active')) {
            $(this).removeClass('is-active');
        } else {
            var hasMultipleChoices = $(this).closest('.calculator-section')[0].hasAttribute('multiple');

            if (!hasMultipleChoices) {
                activeElements.removeClass('is-active');
            }

            $(this).addClass('is-active');
        }
    }

    /**
     * Клик на кнопки этажей
     */
    function floorsButtonsClick() {
        if (!$(this).hasClass('is-active')) {
            var floor = +$(this).attr('floor');
            var section = $(this).closest('.calculator-section');
            var stage = section.attr('stage');

            section.find('.calculator-section__floor-control.is-active').removeClass('is-active');

            $(this).addClass('is-active');

            var currentItem = appState.getWithFloor(stage, floor - 1);
            section.find('.panel').removeClass('is-disabled');

            if (currentItem) {
                var base = currentItem.startsWith('п') || currentItem.startsWith('ф') ? 'base' : 'material';
                section.find('.panel.is-active').removeClass('is-active');
                section.find('[' + base + '=' + currentItem + ']').addClass('is-active');
            } else {
                section.find('.panel.is-active').removeClass('is-active');
            }

            steps.getStageByName(stage).checkCompability();
        }
    }

    /**
     * Инициализация глобальных событий в калькуляторе
     */
    function init() {
        $('body').on('click', '.panel.is-selectable', toggleButtons);

        $('[base], [material], [state]').each(function () {
            setState.call(this);
        });

        $('body').on('click', '.calculator-section__floor-control', floorsButtonsClick);

        $('.calculator-section').on('click', '.calculator-section__header', function (e) {
            if ($(window).width() < 1020) {
                $(e.delegateTarget).find('.calculator-section__inner').slideToggle(300);
            }
        });

        /**
         * Создает контекст для вывода и печати
         * @return {object}
         */
        function makeSmetaContext() {
            var wide = appState.getIndex('wide');
            var floors = appState.getIndex('floors');
            var length = appState.getIndex('length');

            var mansardType = calcState['mansard'][1];

            if (mansardType) {
                mansardType = mansardType.startsWith('Мансардный') ? 'Деревянная' : mansardType;
            } else {
                mansardType = 'Нет';
            }

            var context = {
                'headers': ['Элемент строения', 'Вид элемента строения', 'Доп. характеристики', 'Стоимость'],
                'wide': wide,
                'length': length,
                'floors': floors,
                'area': wide * length * floors,
                'mansard': mansardType,
                'date': new Date(Date.now()).toLocaleString(),
                'totalPrice': getPrice(),
                'remoteness': appState.getIndex('remoteness'),
                'data': calcState
            };

            return context;
        }

        $('body').on('click', '.calc-table-popup__print-link', function (e) {
            var context = makeSmetaContext();

            $.ajax({
                url: 'php/smeta.php',
                type: 'POST',
                data: {
                    'data': JSON.stringify(context)
                },
                success: function success(filename) {
                    window.location = 'php/download.php?filename=' + filename;
                },
                error: function error() {
                    console.log('Ошибка! Попробуйте позже.');
                }
            });
        });

        $('#btn-smeta').on('click', function (e) {
            e.preventDefault();
            var source = document.getElementById('smeta-template').innerHTML;
            var template = Handlebars.compile(source);
            var context = makeSmetaContext();

            var el = $(template(context));

            $.magnificPopup.open({
                items: {
                    src: el,
                    type: 'inline'
                },
                fixedContentPos: true,
                closeBtnInside: false,
                preloader: false,
                midClick: true,
                fixedBgPos: true,
                mainClass: 'my-mfp-slide-bottom smeta-popup'
            });
        });
    }

    var costsCoeff = 1.12;

    var wide = void 0;
    var length = void 0;
    var floorsCount = void 0;
    var areaCoeff = void 0;
    var isMansard = void 0;
    var remoteCoeff = void 0;

    /**
     * Считает коэффициент удаленности
     * @return {float}
     */
    function calculateRemoteCoeff() {
        return +appState.getIndex('remoteness') / 20 || 1;
    }

    /**
     * Устанавливает базовые значения
     */
    function setDefaults() {
        wide = appState.getIndex('wide');
        length = appState.getIndex('length');
        floorsCount = appState.getIndex('floors');
        isMansard = appState.getIndex('mansard') ? 1 : 0;

        areaCoeff = areaDependency.getCompability(wide * length * (floorsCount || 1));
        remoteCoeff = calculateRemoteCoeff();
    }

    /**
     * Цена фундамента
     * @return {float}
     */
    function calculateFoundation() {
        setDefaults();

        var base = appState.getValue('base');
        var totalPrice = Math.floor(wide * length * Prices[base]['price'][0] * areaCoeff * remoteCoeff * costsCoeff);

        calcState['base'].splice(1, 2);
        calcState['base'].push(Prices[base]['title']);
        calcState['base'].push(Math.floor(totalPrice));

        return Math.floor(totalPrice);
    }

    /**
     * Подсчет стоимости стен
     * @param  {string} type
     * @return {integer}
     */
    function calculateWalls(type) {
        setDefaults();
        var walls = appState.getValue(type);
        var totalPrice = 0;

        var lastTitle = '';
        var lastType = '';

        walls.forEach(function (item, index) {
            if (item) {
                var overlapping = appState.getValue('overlapping')[index];
                var base = appState.getValue('base');

                var armCoeff = 1;
                var isLastFloor = index == floorsCount - 1;

                if (overlapping) {
                    var compabilityByOverlapping = Compability.getMaterial(item)['compability'][overlapping];
                    if (compabilityByOverlapping == '***') {
                        armCoeff = Compability.getMaterial(item)['compability'][base];
                    }
                }

                if (isLastFloor) {
                    armCoeff = Compability.getMaterial(item)['compability'][base];

                    var currentWallsPrice = wide * length * Prices[item]['price'][0] * armCoeff * areaCoeff * remoteCoeff * costsCoeff;

                    totalPrice += currentWallsPrice;
                } else {
                    totalPrice += wide * length * Prices[item]['price'][0] * areaCoeff * remoteCoeff * costsCoeff * armCoeff;
                }

                lastTitle = Prices[item]['title'];
                lastType = Prices[item]['type'];
            }
        });

        calcState[type] = calcState[type].splice(0, 1);

        if (lastTitle) {
            calcState[type].push(lastTitle);
            calcState[type].push(Math.floor(totalPrice));
            if (lastType) {
                calcState[type].push(lastType);
            }
        }

        return Math.floor(totalPrice);
    }

    /**
     * Расчет цены перекрытий
     * @return {integer}
     */
    function calculateOverlapping() {
        setDefaults();
        var totalPrice = 0;
        var overlapping = appState.getValue('overlapping');

        var lastTitle = '';

        overlapping.forEach(function (item) {
            if (item) {
                totalPrice += wide * length * Prices[item]['price'][0] * areaCoeff * remoteCoeff * costsCoeff;

                lastTitle = Prices[item]['title'];
            }
        });

        calcState['overlapping'].splice(1, 2);

        if (lastTitle) {
            calcState['overlapping'].push(lastTitle);
            calcState['overlapping'].push(Math.floor(totalPrice));
        }

        return totalPrice;
    }

    /**
     * Расчет мансарды
     * @return {float}
     */
    function calculateMansard() {
        setDefaults();
        var totalPrice = 0;
        var mansardMaterial = appState.getIndex('mansardMaterial');
        var innerWalls = appState.getValue('innerWalls');
        var outerWalls = appState.getValue('outerWalls');

        if (mansardMaterial == 'м0047') {
            var wallPrice = Prices[mansardMaterial]['price'][1];
            totalPrice += wide * length * wallPrice * areaCoeff * remoteCoeff * costsCoeff;
        } else {
            var outerWallPrice = Prices[outerWalls[outerWalls.length - 1]]['price'][1];
            totalPrice += wide * length * outerWallPrice * areaCoeff * remoteCoeff * costsCoeff;

            // TODO: fix null в первом значении
            if (innerWalls.length && innerWalls[0]) {
                var innerWallsPrice = Prices[innerWalls[innerWalls.length - 1]]['price'][1];
                totalPrice += wide * length * innerWallsPrice * areaCoeff * remoteCoeff * costsCoeff;
            }
        }

        calcState['mansard'].splice(1, 2);

        if (mansardMaterial == 'м0047') {
            calcState['mansard'].push(Prices['м0047']['title']);
            calcState['mansard'].push(Math.floor(totalPrice));
        } else {
            calcState['mansard'].push(Prices[outerWalls[outerWalls.length - 1]]['title']);
            calcState['mansard'].push(Math.floor(totalPrice));
        }

        return totalPrice;
    }

    /**
     * Считаем простые этапы
     * @return {float}
     */
    function calculateDefaults() {
        setDefaults();
        var totalPrice = 0;

        ['facade', 'siding', 'roof', 'networks'].forEach(function (stage) {
            var val = appState.getValue(stage);
            var currentPrice = 0;

            if (val) {
                if (Array.isArray(val)) {
                    calcState[stage][1] = [];
                    val.forEach(function (item) {
                        currentPrice += wide * length * Prices[item]['price'][isMansard] * areaCoeff * costsCoeff * remoteCoeff;
                        calcState[stage][1].push([Prices[item]['title'], Math.floor(currentPrice)]);
                    });
                } else {
                    calcState[stage].splice(1, 2);
                    currentPrice += wide * length * Prices[val]['price'][isMansard] * areaCoeff * costsCoeff * remoteCoeff;
                    calcState[stage].push(Prices[val]['title']);
                    calcState[stage].push(Math.floor(currentPrice));
                }
            }

            totalPrice += currentPrice;
        });

        return totalPrice;
    }

    /**
     * Расчет стоимости окон
     * @return {float}
     */
    function calculateWindows() {
        setDefaults();
        var totalPrice = 0;

        var priceKeys = {
            'windows_1': 'дов0043',
            'windows_2': 'дов0044',
            'windows_3': 'дов0045'
        };

        var windows = appState.getValue('windows');

        ['windows_1', 'windows_2', 'windows_3'].forEach(function (key) {
            var val = windows[key];
            var priceKey = priceKeys[key];

            if (val) {
                var currentPrice = val * Prices[priceKey]['price'][isMansard];
                calcState['windows'][1][key][1] = Math.floor(currentPrice);
                totalPrice += currentPrice;
            } else {
                calcState['windows'][1][key][1] = 0;
            }
        });

        return totalPrice;
    }

    /**
     * Расчет утепления
     * @return {float}
     */
    function calculateWarming() {
        setDefaults();
        var totalPrice = 0;
        var warming = appState.getValue('warming');

        Object.entries(warming).forEach(function (item) {
            var _item = _slicedToArray(item, 2),
                key = _item[0],
                val = _item[1];

            calcState['warming'][1][key]['price'] = [];

            if (val) {
                val.forEach(function (subitem, index) {
                    var currentPrice = wide * length * floorsCount * Prices[subitem]['price'][isMansard];
                    if (subitem) {
                        totalPrice += currentPrice;
                        var type = Prices[subitem]['type'];

                        calcState['warming'][1][key]['price'][index] = [Math.floor(currentPrice), type];
                    }
                });
            }
        });

        return totalPrice;
    }

    /**
     * Расчет цены дверей
     * @return {float}
     */
    function calculateDoors() {
        var door = appState.getValue('doors');

        calcState['doors'].splice(1, 2);

        if (door) {
            calcState['doors'][1] = Prices[door]['title'];
            calcState['doors'][2] = Prices[door]['price'][isMansard];
        } else {
            return 0;
        }

        return Prices[door]['price'][isMansard];
    }

    var totalPrice = 0;

    /**
     * Рассчет цены строительства.
     */
    function calculatePrice() {
        totalPrice = 0;

        if (appState.getValue('base')) {
            totalPrice += Math.floor(calculateFoundation());
        } else {
            calcState['base'].splice(1, 2);
        }

        [calculateWalls.bind(null, 'outerWalls'), calculateWalls.bind(null, 'innerWalls'), calculateWindows, calculateWarming, calculateDefaults, calculateOverlapping].forEach(function (func) {
            return totalPrice += Math.floor(func());
        });

        if (appState.getIndex('mansardMaterial')) {
            totalPrice += Math.floor(calculateMansard());
        } else {
            calcState['mansard'].splice(1, 2);
        }

        var gate = appState.getValue('gate');

        calcState['gate'].splice(1, 2);

        if (gate) {
            calcState['gate'].push('Ворота');
            calcState['gate'].push(Prices['дов0046']['price'][0]);

            totalPrice += Prices['дов0046']['price'][0];
        }

        totalPrice += calculateDoors();

        renderTotalPrice(Math.floor(totalPrice));
    }

    /**
     * Возврат итоговой суммы
     * @return {integer}
     */
    function getPrice() {
        return totalPrice;
    }

    return {
        'init': init,
        'calculateWindows': calculateWindows,
        'calculateMansard': calculateMansard,
        'calculateFoundation': calculateFoundation,
        'calculatePrice': calculatePrice,
        'setState': setState,
        'getPrice': getPrice
    };
}, appState);