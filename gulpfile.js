var gulp         = require('gulp'),
    babel        = require('gulp-babel'),
    browserSync  = require('browser-sync'),
    uglify       = require('gulp-uglifyjs'),
    cssnano      = require('gulp-cssnano'),
    gulpIf       = require('gulp-if'),
    useref       = require('gulp-useref'),
    plumber      = require('gulp-plumber'),
    autoprefixer = require('gulp-autoprefixer'),
    ftp          = require( 'vinyl-ftp' ),
    del          = require('del'),
    imagemin     = require('gulp-imagemin'),
    cache        = require('gulp-cache'),
    cached       = require('gulp-cached'),
    pngquant     = require('imagemin-pngquant'),
    spritesmith  = require('gulp.spritesmith'),
    stylus       = require('gulp-stylus'),
    sourcemaps   = require('gulp-sourcemaps'),
    pugbem       = require('gulp-pugbem'),
    pug          = require('gulp-pug');


// build

gulp.task('clean', function(){
    return del.sync('dist');
});

gulp.task('optimazeimg', function(){
    return gulp.src('app/img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        })))
        .pipe(gulp.dest('dist/img'))
});

// gulp.task('clearCache', function(){
//     return cache.clearAll();
// });

gulp.task('useref', function(){
    return gulp.src('app/*.html')
        .pipe(useref())
        .pipe(
            gulpIf(['*.js', '!scripts.min.js'], uglify())
        )
        .pipe(gulpIf(['*.css','!style.min.css'], cssnano()))
        .pipe(gulp.dest('dist'));
});

gulp.task('copyFiles', function(){
    return gulp.src(['app/fonts/**/*', 'app/css/**/*', 'app/php/**/*'], {base:"app"})
        .pipe(gulp.dest('dist'))
});

// usefull

gulp.task('sprite', function () {
    var spriteData = gulp.src('app/img/sprite/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.scss'
    }));
    return spriteData.pipe(gulp.dest('app/img/sprite'));
});

// general

gulp.task('pug', function(){
    return gulp.src(['app/pug/**/*.pug','!app/pug/**/_*.pug'])
        .pipe(plumber())
        .pipe(pug({
            pretty: true,
            plugins: [pugbem]
        }))
        .pipe(gulp.dest('dist'))
});

// stylus

gulp.task('stylus', function(){
    gulp.src(['app/stylus/**/*.styl','!app/stylus/**/_*.styl'])
        .pipe(plumber())
        .pipe(stylus({
            'include css': true
        }))
        .pipe(autoprefixer(['last 2 versions', '> 5%', 'Firefox ESR' , 'ie 9']))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('js', function () {
    return gulp.src('app/js/**/' + '*.js')
        .pipe(babel())
        .pipe(gulp.dest('dist/js/'))
        .pipe(browserSync.reload({
          stream: true
        }));
});

// sync

gulp.task('browser-sync', function(){
    browserSync({
        server: {
            baseDir: 'dist',
            proxy: 'welbit.test'
        },
        logLevel: 'debug'
    });
});

// watch

gulp.task('build', ['pug', 'stylus', 'js', 'copyFiles'], function(){
});

gulp.task('default', ['browser-sync', 'stylus', 'pug'], function(){
    gulp.watch('app/pug/**/*.pug', ['pug']);
    gulp.watch(['app/stylus/**/*.styl','app/stylus/**/_*.styl'], ['stylus']);
    gulp.watch('dist/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', ['js']);
});


// deploy

gulp.task('deploy', ['build'], function(){

    var ftpSettings = require('./ftp-deploy-settings.js');
    var conn = ftp.create( ftpSettings.settings.auth );

    var globs = [
        'dist/**/*.*'
    ];

    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance

    return gulp.src( globs, { base: 'dist', buffer: false } )
        .pipe( conn.newer( '/' ) ) // only upload newer files
        .pipe( conn.dest( ftpSettings.settings.path ) );

});
