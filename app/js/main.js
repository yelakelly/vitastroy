$(document).ready(function() {
    let defaultPopupOptions = {
        type: 'inline',
        fixedBgPos: true,
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
    };

    /**
     * Скролл к элементу полученного из href ссылки
     * @param  {el} item
     * @param  {integer} offset
     */
    function scrollTo(item, offset) {
        offset = offset || 0;

        $('html, body').animate({
            scrollTop: $(item).offset().top - offset,
        }, 1000);
    }

    /**
     * @param  {html} top
     * @param  {html} bottom
     * @param  {integer} screenSize - максимальный размер экрана
     */
    function repositionBlocks(top, bottom, screenSize) {
        $(window).width() < screenSize ? top.insertAfter(bottom) : bottom.insertAfter(top);
    }

    $('body').on('click', '.js-scroll-to', function() {
        let elem = $(this).attr('href');
        let offset = $('.site-header__helmet').height();

        elem.match(/#.+/) && scrollTo(elem, offset);
    });

    $('body').on('click', '.js-nav-toggle, .js-scroll-to', () => {
        $('.js-nav-toggle').toggleClass('is-active');
        $('.primary-nav--mobile').toggleClass('is-disable');
    });

    let elHeader = $('.site-header__helmet');

    /**
     * @param  {html} elHeader
     */
    function setFixedHeader(elHeader) {
        $('body').css('padding', elHeader.outerHeight(true) + 'px 0 0 0');
        $('body').addClass('has-fixed-header');
    }

    /**
     * @param  {html} elHeader
     */
    function unsetFixedHeader(elHeader) {
        $('body').removeClass('has-fixed-header');
        $('body').attr('style', '');
    }

    /**
     * Устанавливает фиксированную шапку при скролле
     * @param  {html} el
     * @param  {integer} threshold - отступ
     */
    function setFixedHeaderOnScroll(el, threshold) {
        let onlyOnceFlag = false;

        $(window).scroll(function() {
            let isTrue = $(window).scrollTop() >= threshold;

            if (isTrue && !onlyOnceFlag) {
                setFixedHeader(el);
                setTimeout(function() {
                    el.css('display', 'block');
                }, 50);
                onlyOnceFlag = true;
            } else if (!isTrue && onlyOnceFlag) {
                unsetFixedHeader(el);
                onlyOnceFlag = false;
            }
        });
    }

    if ($(window).width() < 800) {
        setFixedHeaderOnScroll(elHeader, elHeader.offset().top);
    }

    /**
     * @param  {html} menuInside
     * @return {html}
     */
    function changeMenuPosition(menuInside) {
        if ($(window).width() < 1120 && !menuInside) {
            menuInside = true;
            $('.primary-nav__list')
                .remove()
                .prepend(`<div class="primary-nav-button">
                    <button class="hamburger hamburger--collapse js-nav-toggle">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                    </button>
                    </div>`
                )
                .appendTo('body')
                .wrap('<div class="primary-nav--mobile is-disable"></div>');
        }

        if ($(window).width() > 1120 && menuInside) {
            menuInside = false;
            if ($('.primary-nav--mobile').length) {
                $('.js-nav-toggle').removeClass('is-active');
                $('.primary-nav--mobile')
                .find('.primary-nav-button')
                .remove('');

                $('.primary-nav__list')
                    .unwrap()
                    .remove()
                    .appendTo('.primary-nav');
            }
        }
        return menuInside;
    }

    $('.input-wrapper-controls__up').on('click', function() {
        let _input = $(this).closest('.input-wrapper__counter').find('input');
        _input.val((+_input.val() || 0) + 1);
        _input.trigger('change');
    });

    $('.input-wrapper-controls__down').on('click', function() {
        let _input = $(this).closest('.input-wrapper__counter').find('input');
        let _num = +_input.val();

        _input.val(_num && _num > -1 ? _num - 1 : 0);
        _input.trigger('change');
    });

    $('input[type="tel"]').mask('+7-(999)-999-99-99');

    let menuInside = false;

    menuInside = changeMenuPosition(menuInside);
    repositionBlocks($('.calculator-section__area-total-wrapper'),
    $('.calculator-section__col-w-field--remoteness'), 500);

    $(window).resize(function() {
        menuInside = changeMenuPosition(menuInside);
        repositionBlocks($('.calculator-section__area-total-wrapper'),
        $('.calculator-section__col-w-field--remoteness'), 500);
    });

    $('.work-types .ui-col__btn').on('click', function(e) {
        e.preventDefault();
        let text = $(this).siblings('.ui-col__text').html();

       if ($(window).width() > 500) {
           $.magnificPopup.open($.extend(defaultPopupOptions, {
               items: {
                   src: '#callback-form',
                   type: 'inline',
               },
           }));
       } else {
           $.magnificPopup.open({
               items: {
                   src: `<div class="popup-text-container">${text}</div>`,
                   type: 'inline',
               },
               closeBtnInside: true,
           });
       }
    });

    let messagePopupSettings = {
        type: 'inline',
        fixedBgPos: true,
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom',
        fixedContentPos: true,
        showCloseBtn: false,
    };

    $('.js-send-form').submit(function(e) {
        let postData = $(this).serializeArray();
        let formURL = $(this).attr('action');

        let requiredFields = $(this).find('[data-required]');
        let isError = false;

        requiredFields.removeClass('is-error');

        requiredFields.each(function() {
            $(this).val() ? $(this).addClass('is-error') && (isError = true) : null;
        });

        if (formSource) {
            postData.push({
              'name': 'source',
              'value': formSource,
            });
        }

        if (isError) return false;

        let form = $(this);

        $.ajax({
            url: formURL,
            type: 'POST',
            data: postData,
            success: function(data) {
                $.magnificPopup.close();

                setTimeout(function() {
                    $.magnificPopup.open($.extend(messagePopupSettings, {
                        items: {
                            src: $('#message-popup'),
                        },
                    }));
                }, 300);

                form.find('input[type="text"], input[type="tel"], textarea').val('');
            },
            error: function() {
                $.magnificPopup.close();

                setTimeout(function() {
                    $.magnificPopup.open($.extend(messagePopupSettings, {
                        items: {
                            src: $('#error-popup'),
                        },
                    }));
                }, 300);
            },
        });
        e.preventDefault();
    });

    // Magnific popups close event

    $('.js-close-button').on('click', function() {
        $.magnificPopup.close();
    });

    let initialFormHeader;
    let formSource;

    $('.js-open-popup').magnificPopup($.extend(defaultPopupOptions, {
        fixedContentPos: true,
        showCloseBtn: false,
        callbacks: {
            open: function() {
                let mp = $.magnificPopup.instance;
                let button = $(mp.st.el);
                let _form = $(mp)[0].content;

                // TODO: сделать более гибким

                let formHeader = button.data('title');
                formSource = button.data('source');
                initialFormHeader = _form.find('.default-form__text-header').html();

                formHeader ? _form.find('.default-form__text-header').html(formHeader) : null;
            },
            close: function() {
                let mp = $.magnificPopup.instance;
                let _form = $(mp)[0].content;
                formSource = '';

                initialFormHeader ? _form.find('.default-form__text-header').html(initialFormHeader) : null;
            },
        },
        mainClass: 'my-mfp-slide-bottom',
    }));

    $('.has-open-image').on('click', function(e) {
        e.preventDefault();

        let mp = $.magnificPopup.instance;
        let button = $(mp.st.el);

        if (mp.isOpen) {
            mp.close();
        }

        let self = $(this);

        setTimeout(function() {
            $.magnificPopup.open({
                items: {
                    'src': self.attr('href'),
                },
                callbacks: {
                    'afterClose': function() {
                        button
                            .magnificPopup('open', $('.js-open-popup')
                            .index(button));
                    },
                },
                closeOnContentClick: false,
                closeBtnInside: false,
                type: 'image',
            });
        }, 300);
    });

    /** Устанавливает событие на открытие галереи **/
    function setGalleryClick() {
        $('body').on('click', '.has-open', function(e) {
            let currentTarget = $(e.currentTarget);
            e.preventDefault();

            let mp = $.magnificPopup.instance;
            let button = $(mp.st.el);

            if (mp.isOpen) {
                mp.close();
            }

            let container = $(this).closest('.single-popup__left');
            let currentIndex = currentTarget.index('.has-open');

            let images = container
                .find('.has-open')
                .map(function() {
                    return {
                        src: $(this).attr('href'),
                    };
                })
                .get();

            setTimeout(function() {
                $.magnificPopup.open({
                    items: images,
                    callbacks: {
                        'afterClose': function() {
                            button
                            .magnificPopup('open', $('.js-open-popup')
                            .index(button));
                        },
                    },
                    closeOnContentClick: false,
                    closeBtnInside: false,
                    mainClass: 'my-mfp-slide-bottom',
                    gallery: {
                        enabled: true,
                        tCounter: '%curr% из %total%',
                    },
                    type: 'image',
                });
                $.magnificPopup.instance.goTo(currentIndex);
            }, 300);
        });
    }

    setGalleryClick();

    $('.project-popup__scrollable').scrollbar({
        'scrollx': 'none',
    });

    // $('.projects-frame').contents().find('body').css({
    //     'background-color': '#fafbff',
    // });

    // let slider = document.getElementById('project-slider');

    // noUiSlider.create(slider, {
    //     start: [40, 420],
    //     format: wNumb({
    //         decimals: 0,
    //     }),
    //     tooltips: [true, true],
    //     connect: true,
    //     range: {
    //         'min': 40,
    //         'max': 420,
    //     },
    // });
});
