/** Класс представляет этап в калькуляторе. */
class Stage {
    /**
     * @param  {object} settings
     */
    constructor(settings) {
        this.el = settings['el'];
        this.title = settings['title'];
        this.name = settings['name'];
        this.nextStep = settings['nextStep'];
        this.prevStep = settings['prevStep'];
        this.onActive = settings['onActive'];
        this.onDisable = settings['onDisable'];
        this.compability = settings['compability'];
    }

    /** Инициализация */
    init() {
        if (!this.initialize) {
            this.setEvents();
        }

        this.initialize = true;
    }

    /** Установка событий */
    setEvents() {}

    /** Очищение состояния */
    setClear() {
        let val = appState.getValue(this.name);

        if (Array.isArray(val)) {
            appState.setValue(this.name, []);
        } else if (typeof val == 'string') {
            appState.setValue(this.name, '');
        } else if (!isNaN(val)) {
            val = 0;
        }

        $(this.el).find('.panel.is-active').removeClass('is-active');
    }

    /** Отключает этап */
    setDisabled() {
        $(this.el).find('.is-disable').removeClass('is-disable');
        $(this.el).addClass('not-active');
    }

    /** Проверяет совместимость **/
    checkCompability() {
        // Получаем текущие материалы
        // Проверяем совместимость и выключаем несовместимые.
        let base = appState.getValue('base');
        let currentCols = $(this.el).find('.panel.is-selectable');
            currentCols.removeClass('is-disable');

        if (base) {
            // Получаем базовые значение

            let rules = [base];
            let floorsCount = appState.getIndex('floors');
            

            let bannedMaterials = [];

            let disabledButtons = currentCols.filter(function() {
                let isActive;
                let material = $(this).attr('material') || $(this).attr('base');

                if (material.startsWith('ф')) {
                    rules = [...appState.getValue('innerWalls'), ...appState.getValue('outerWalls')];
                    isActive = Compability.getCompabilityByBase(material, rules, floorsCount);
                } else {
                    isActive = Compability.getCompability(material, rules, floorsCount);
                }

                if (!isActive) {
                    bannedMaterials.push(material);
                }

                return !isActive;
            });

            if (bannedMaterials.length) appState.clearByName(this.name, bannedMaterials);

            disabledButtons.removeClass('is-active').addClass('is-disable');
        } else if (this.name == 'base') {
            let floors = +$('#floors').val();

            if (floors > 1) {
                $('[base="ф0001"]').addClass('is-disable');
            }
        }
    }

    /** Делает этап активным */
    setActive() {
        if (this !== mansardStage && this.compability !== false) {
            this.checkCompability();
        }
        
        $(this.el).addClass('is-active');
        $(this.el).removeClass('not-active');
    }

    /** Переход к следующим этапам */
    goToNextStep() {
        let nextStep = this.nextStep;
        let currentStage = steps.getCurrentStage();

        if (nextStep && nextStep !== currentStage) {
            let prevStage = steps.getPrevStage();

            if (prevStage) {
                prevStage.setDisabled();
            }

            if (Array.isArray(nextStep)) {
                nextStep.forEach((stage) => steps.addNewStage(stage));
            } else {
                steps.addNewStage(nextStep);
            }
        }

        if (currentStage.onActive) {
            currentStage.onActive();
        }
    }

    /** Делает переход к предыдущему этапу */
    goToPrevStep() {
        let currentStage = steps.getCurrentStage();
        let prevStep = currentStage.prevStage;

        if (prevStep !== false) {
            steps.setPrevStage(prevStep);
        }

        if (currentStage.onDisable) {
            currentStage.onDisable();
        }
    }
}

let steps = (function() {
    let stages = [];

    /**
      * Функция получает этап по имени
      * @param {string} name - имя этапа
      * @return {object} Stage instance
    **/
    function getStageByName(name) {
        for (let i = 0; i < stages.length; i++) {
            if (stages[i].name == name) {
                return stages[i];
            }
        }
    }

    /**
      * Функция устанавливает активным предыдущий этап
    **/
    function setPrevStage() {
        let currentStage = getCurrentStage();
        currentStage.setDisabled();
        removeLastStage();

        let prevStage = getPrevStage();
        prevStage.setActive();
    }

    /**
      * Удаляет последний этап
    **/
    function removeLastStage() {
        stages.splice(-1, 1);
    }

    /**
      * Функция добавляет новый этап и активирует его.
      * @param {Stage} stage
    **/
    function addNewStage(stage) {
        stage.init();
        stages.push(stage);
        
        if (stage.onActive) {
            stage.onActive();
        }

        stage.setActive();
    }

    /**
      * Функция возвращает текущий этап
      * @return {Stage} Stage instance
    **/
    let getCurrentStage = () => stages[stages.length - 1];

    let getPrevStage = () => stages[(stages.length - 1) - 1] || false;

    /**
      * Функция возвращает все этапы
      * @return {array} Массив всех этапов
    **/
    let getStages = () => stages;

    /**
     * Удаляет шаг
     * @param  {Stage} stage
     */
    function removeStage(stage) {
        stages.forEach(function(st, index) {
            if (st == stage) {
                stages.splice(index, 1);
            }
        });
    }

    return {
        'addNewStage': addNewStage,
        'setPrevStage': setPrevStage,
        'getPrevStage': getPrevStage,
        'getCurrentStage': getCurrentStage,
        'getStageByName': getStageByName,
        'getStages': getStages,
        'removeStage': removeStage,
    };
})();

let networksStage = new Stage({
    'el': $('.calculator-section--networks'),
    'title': 'Инженерные сети',
    'name': 'networks',
    'prevStep': null,
});

let facadeStage = new Stage({
    'el': $('.calculator-section--facade'),
    'title': 'Фасад',
    'name': 'facade',
    'prevStep': null,
});

let warmingStage = new Stage({
    'el': $('.calculator-section--warming'),
    'title': 'Утепление',
    'name': 'warming',
    'prevStep': null,
});

let otherStage = new Stage({
    'el': $('.calculator-section--windows'),
    'title': 'Окна',
    'name': 'windows',
    'prevStep': null,
});

let sidingStage = new Stage({
    'el': $('.calculator-section--siding'),
    'title': 'Сайдинг',
    'name': 'siding',
    'prevStep': null,
    'compability': false,
});

let roofStage = new Stage({
    'el': $('.calculator-section--roof'),
    'title': 'Кровля',
    'name': 'roof',
    'prevStep': false,
});

let mansardStage = new Stage({
    'el': $('.calculator-section--mansard'),
    'title': 'Мансарда',
    'name': 'mansard',
    'nextStep': roofStage,
    'compability': false,
});

mansardStage.goToNextStep = function() {
    let mansardMaterial = appState.getIndex('mansardMaterial');

    if (mansardMaterial !== 'м0047') {
        this.__proto__.goToNextStep.call(this);
    } else {
        roofStage.setClear();
        let currentStage = steps.getCurrentStage();

        if (currentStage !== this) {
            steps.setPrevStage();
        }
    }
};

mansardStage.onActive = function() {
    if ($(this.el).find('.panel').length == 2) {
        return;
    }

    let wallMaterial = appState.getValue('outerWalls');
    let value = wallMaterial[wallMaterial.length - 1];
    let container = $(this.el).find('.calculator-section__container');

    $(renderPanel({
        'type': 'material',
        'value': value,
        'event': 'setMansardMaterial',
        'state': 'mansard',
        'title': materialsInfo[value].title,
        'img': materialsInfo[value].img,
    })).appendTo(container);

    let column = container.find('.panel').last();
    App.setState.call(column);
};

mansardStage.onDisable = function() {
    let panels = $(this.el).find('.calculator-section__col');
    if (panels.length > 1) {
        panels.last().remove();
    }
};

let overlappingStage = new Stage({
    'el': $('.calculator-section--overlapping'),
    'title': 'Перекрытия',
    'name': 'overlapping',
    'nextStep': roofStage,
});

overlappingStage.checkCompability = function() {
    let currentFloor = $(this.el)
                        .find('.calculator-section__floor-control.is-active')
                        .attr('floor');
    currentFloor = !isNaN(currentFloor) ? currentFloor - 1 : 1;
    const floorsCount = appState.getIndex('floors');

    let inWalls;
    let exWalls;

    if (currentFloor) {
        inWalls = appState.getValue('innerWalls')[currentFloor - 1];
        exWalls = appState.getValue('outerWalls')[currentFloor - 1];
    } else {
        inWalls = appState.getValue('innerWalls')[currentFloor];
        exWalls = appState.getValue('outerWalls')[currentFloor];
    }
    
    let _bases = [];

    let currentCols = $(this.el).find('.panel.is-selectable');
        currentCols.removeClass('is-disable');

    if (inWalls) _bases.push(inWalls);
    if (exWalls) _bases.push(exWalls);

    let disabledButtons = currentCols.filter(function() {
        let isActive;
        let material = $(this).attr('material') || $(this).attr('base');

        isActive = Compability.getCompabilityByBase(material, _bases, floorsCount, currentFloor + 1);

        return !isActive;
    });

    disabledButtons.removeClass('is-active').addClass('is-disable');
};

let innerWalls = new Stage({
    'el': $('.calculator-section--inner-walls'),
    'title': 'Внутренние стены',
    'name': 'innerWalls',
    'nextStep': overlappingStage,
});

let outerWalls = new Stage({
    'el': $('.calculator-section--external-walls'),
    'title': 'Внешние стены',
    'name': 'outerWalls',
    'nextStep': innerWalls,
});

let foundationStage = new Stage({
    'el': $('.calculator-section--foundation'),
    'title': 'Тип фундамента',
    'name': 'base',
    'nextStep': outerWalls,
    'onActive': function() {
        let activeSteps = [
            networksStage,
            facadeStage,
            warmingStage,
            otherStage,
            sidingStage,
        ];

        activeSteps.forEach(function(step) {
            step.setActive();
        });
    },
    'onDisable': function() {
        let activeSteps = [
            networksStage,
            facadeStage,
            warmingStage,
            otherStage,
            sidingStage,
        ];

        activeSteps.forEach(function(step) {
            step.setDisabled();
        });
    },
});

foundationStage.checkCompability = function() {
    this.__proto__.checkCompability.call(this);

    [roofStage, facadeStage].forEach(function(step) {
        step.checkCompability();
    });
};

let indexStage = new Stage({
    'el': $('.calculator-section--dimensions'),
    'title': 'Параметры строения',
    'name': 'dimensions',
    'nextStep': foundationStage,
});

indexStage.setEvents = function() {
    let self = this;
    let el = $(this.el);
    
    // TODO: сделать более универсально

    $('#floors').on("keyup change", function() {
        let value = this.value;

        if (value == '' || isNaN(value)) {
            return;
        }
        
        value = (+value);

        if (value > 3) {
            value = 3;
            this.value = 3;
        }

        if (value <= 0) {
            value = 1;
            this.value = 1;
        }
        
        if (value > 1) {
            $('.has-floors .calculator-section__controls')
                .html(renderFloors(value));
        } else {
            // fix нуллей в стенах
            ['innerWalls', 'outerWalls', 'overlapping']
                .forEach((key) => appState.setValue(key, []));

            $(outerWalls.el, innerWalls.el, overlappingStage.el)
                .find('.panel.is-active')
                .removeClass('is-active');

            $('.has-floors .calculator-section__controls').html('');
        }

        value > 1 ? $('[base="ф0001"]').addClass('is-disable') : $('[base="ф0001"]').removeClass('is-disable');
    });

    // Выводим площадь при изменении базовых значений

    el.find('#wide, #length, #floors, #walls, #remoteness').each(function() {        
        $(this).on('keyup change', () => {
            
            el.find('input[name="walls"]').val(self.calculateInnerWalls);
            el.find('.calculator-section__area-total > span').last().html(
                `${self.calculateArea()}<span>м<sup>2</sup></span>`
            );
        });
        App.calculatePrice();
    });

    // При заполнении всех полей, делаем доступным блок Фундамент

    // Вынести в метод

    $('#wide, #length, #floors').on('keyup change', function() {
        let requiredCount = 0;

        $('#wide, #length, #floors').each(function() {
            $(this).val() ? requiredCount++ : null;
        });

        let currentStage = steps.getCurrentStage();

        if (requiredCount == 3 && currentStage !== foundationStage) {
            self.goToNextStep();
        }
        
        if (currentStage == foundationStage && requiredCount < 3) {
            self.goToPrevStep();
        }
    });

    // Блок мансарда

    el.find('#mansard').on('change', function() {
        let name = $(this).attr('name');
        let value = $(this).is(':checked') ? true : false;

        appState.setIndex(name, !!$(this).val());

        if (value) {
            $('.calculator-section--mansard').removeClass('is-hidden');
            overlappingStage.nextStep = mansardStage;
        } else {
            $('.calculator-section--mansard').addClass('is-hidden');
            overlappingStage.nextStep = roofStage;
        }

        calculateSteps();
    });
};

indexStage.calculateArea = () => (+$('#wide').val() * +$('#length').val() * +$('#floors').val()) || 0;
indexStage.calculateInnerWalls = () => (+$('#wide').val() * +$('#length').val() * 0.5) || 0;

steps.addNewStage(indexStage);

App.init();