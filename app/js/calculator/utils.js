/**
 * @module Utils
 */

if (!Object.entries) {
  Object.entries = function(obj) {
      let ownProps = Object.keys(obj);
      let i = ownProps.length;
      let resArray = new Array(i);

      while (i--) {
        resArray[i] = [ownProps[i], obj[ownProps[i]]];
      }

      return resArray;
  };
}

let materialsInfo = {
  'с0006': {
    'title': 'Кирпич 250',
    'img': 'kirpich-250.png',
  },
  'с0007': {
    'title': 'Кирпич 380',
    'img': 'kirpich-380.png',
  },
  'с0008': {
    'title': 'Каркас по <br> системе сендвич',
    'img': 'karkasnie-po-sisteme-sendvich.png',
  },
  'с0009': {
    'title': 'Блок газоселикатный',
    'img': 'penoblock.png',
  },
  'с0010': {
    'title': 'Монолит <br> железобетонный',
    'img': 'monolitnie-zelezobetonnie.png',
  },
  'с0011': {
    'title': 'Керамзито-<br>бетонный блок',
    'img': 'keramzitno-betonny-block.png',
  },
};

 /**
 * Рендер кнопок этажей
 * @param  {object} settings Настройки
 * @return {string}
 */
function renderPanel(settings) {
  let html = `<div class="calculator-section__col">
    <div class="panel is-selectable" event="${settings.event}" state="${settings.state}" 
    event-type="${settings.eventType || 'click'}" 
    ${settings.type || 'material'}="${settings.value}">
      <img class="panel__thumb" src="img/${settings.img}">
      <p class="panel__header"><span>${settings.title}</span></p>
    </div>
  </div>`;

  return html;
}

Handlebars.registerHelper('floor', function(val) {
  val = Handlebars.Utils.escapeExpression(val);

  return new Handlebars.SafeString(Math.floor(val));
});

Handlebars.registerHelper('prettyNumber', function(val) {
  val = Handlebars.Utils.escapeExpression(val);
  return new Handlebars.SafeString(numberWithSpaces(val));
});

function numberWithSpaces(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

function renderTotalPrice(price) {
  price = numberWithSpaces(Math.ceil(price));

  $('.calculator-section__total-price').html(`${price} руб.`);
}

/**
 * Рендер кнопок этажей
 * @param  {integer} floorsCount Количество этажей
 * @return {string}
 */
function renderFloors(floorsCount) {
    let html = '';

    for (let i = 1; i < floorsCount + 1; i++) {
        html += `<span class="calculator-section__floor-control" floor="${i}">${i}й этаж: </span>`;
    }

    // Делаем 1 этаж активным

    html = $(html);
    html.eq(0).addClass('is-active');

    return html;
}

/**
 * Перасчет шагов калькулятора
 */
function calculateSteps() {
    $('.calculator-section').not('.is-hidden').each(function(index) {
        $(this).find('.calculator-section__header span:first-child').html(
           index + 1
        );
    });
}

Handlebars.registerHelper({
  eq: function (v1, v2) {
      return v1 === v2;
  },
  ne: function (v1, v2) {
      return v1 !== v2;
  },
  lt: function (v1, v2) {
      return v1 < v2;
  },
  gt: function (v1, v2) {
      return v1 > v2;
  },
  lte: function (v1, v2) {
      return v1 <= v2;
  },
  gte: function (v1, v2) {
      return v1 >= v2;
  },
  and: function (v1, v2) {
      return v1 && v2;
  },
  or: function (v1, v2) {
      return v1 || v2;
  }
});

// const LOGGER = true;

// let logger = (function(){
//   let logs = [];

//   if (LOGGER) {
//     logs.forEach(function(message){
//       console.log(message);
//     })
//   };

//   function addLogMessage(message) {
//     logs.push(message);
//   }

//   return {
//     'addLogMessage': addLogMessage
//   }
// })();