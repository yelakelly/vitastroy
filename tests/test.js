let assert = require('assert');

let Compability = require('../app/js/calculator/compability');
let areaDependency = require('../app/js/calculator/area-dependency');

let Calculator = require('../app/js/calculator/app');
let CalculatorApp = Calculator.App;
let CalculatorState = Calculator.appState;

describe('Compability', function() {
    describe('#getCompability()', function() {
      it('Cтены Каркасные (СИП) совместимы с Монолит. железоб.', function() {
        assert.equal(1, Compability.getCompability('с0008', ['п0033'], 3));
      });

      it('Cтены Каркасные (СИП) не совместимы с Сборным железоб.', function() {
        assert.equal(0, Compability.getCompability('с0008', ['п0032'], 3));
      });
      
      it('Cтены Каркасные (СИП) совместимы с Дерев. перекр. (2 этаж)', function() {
        assert.equal(1, Compability.getCompability('с0008', ['п0031'], 2, 2));
      });

      it('Cтены Каркасные (СИП) не совместимы с Дерев. перекр. (3 этаж)', function() {
        assert.equal(0, Compability.getCompability('с0008', ['п0031'], 3));
      });
      
    });

    describe('#getCompabilityByBase()', function() {
        it('Перекрытия Дерев. совместимы с блоком газоселикт. (3 этаж)', function() {
            assert.equal(1, Compability.getCompabilityByBase('п0032', ['с0009'], 3));
        });

        it('Перекрытия Дерев. не совместимы c Каркасными (СИП) (3 этаж)', function() {
            assert.equal(0, Compability.getCompabilityByBase('п0031', ['с0008'], 3, 3));
        });

        it('Перекрытия Cборн. желез. не совместимы c Каркасными (СИП)', function() {
            assert.equal(0, Compability.getCompabilityByBase('п0032', ['с0008'], 3, 3));
        });
    });
});

describe('areaDependency', function() {
  describe('#getCompability()', function() {
    it('Верный коэффициент - ожидалось 1.075', function() {
      assert.equal(1.075, areaDependency.getCompability(110.1));
    });

    it('Верный коэффициент - ожидалось 1.15', function() {
      assert.equal(1.15, areaDependency.getCompability(100));
    });

    it('Верный коэффициент - ожидалось 1', function() {
      assert.equal(1, areaDependency.getCompability(150));
    });

    it('Верный коэффициент - ожидалось 0.99', function() {
      assert.equal(0.99, areaDependency.getCompability(171));
    });

    it('Верный коэффициент - ожидалось 1.3', function() {
      assert.equal(1.3, areaDependency.getCompability(40));
    });
  });
});

describe('Calculator', function() {
  describe('#calculateWindows()', function() {
    it('Ожидалось 15000.', function() {
      CalculatorState.setWindow('windows_1', 5);
      assert.equal(15000, CalculatorApp.calculateWindows());
    });

    it('Ожидалось 85000.', function() {
      CalculatorState.setWindow('windows_2', 14);
      assert.equal(85000, CalculatorApp.calculateWindows());
    });

    it('Ожидалось 295000.', function() {
      CalculatorState.setWindow('windows_3', 21);
      assert.equal(295000, CalculatorApp.calculateWindows());
    });
  });

  describe('#calculateFoundation()', function() {
    it('Базовая проверка', function() {
      CalculatorState.setIndex('wide', 10);
      CalculatorState.setIndex('length', 10);
      CalculatorState.setValue('base', 'ф0001');

      assert.equal(Math.floor((10 * 10 * 3097.941176) * 1.15 * 1.12), CalculatorApp.calculateFoundation());
    });

    it('Проверка с удаленностью.', function() {
      CalculatorState.setIndex('wide', 13);
      CalculatorState.setIndex('length', 24);
      CalculatorState.setIndex('floors', 3);
      CalculatorState.setIndex('remoteness', 25);
      CalculatorState.setValue('base', 'ф0003');
      
      assert.equal(Math.floor(((13 * 24) * 4155.262376) * 0.87 * 1.25 * 1.12), CalculatorApp.calculateFoundation());
    });
  });

  describe('#calculateMansard()', function() {
    it('Мансарда из материалов стен', function() {
      CalculatorState.setIndex('wide', 10);
      CalculatorState.setIndex('length', 10);
      CalculatorState.setIndex('remoteness', 0);
      CalculatorState.setIndex('floors', 3);

      [0,1,2].forEach((i) => {
        CalculatorState.setWithFloor('outerWalls', 'с0006', i);
        CalculatorState.setWithFloor('innerWalls', 'с0007', i);
      });

      let outerPrice = (10 * 10 * 4114.479) * 0.97 * 1.12;
      let innerPrice = (10 * 10 * 3542.9025) * 0.97 * 1.12;

      assert.equal(Math.floor(outerPrice + innerPrice), Math.floor(CalculatorApp.calculateMansard()));
    });

    it('Деревянная мансарда.', function() {
      CalculatorState.setIndex('wide', 13);
      CalculatorState.setIndex('length', 24);
      CalculatorState.setIndex('floors', 3);
      CalculatorState.setIndex('remoteness', 25);
      CalculatorState.setIndex('mansardMaterial', 'м0047');
      
      assert.equal(Math.floor(((13 * 24) * 7885.2762) * 0.87 * 1.25 * 1.12), Math.floor(CalculatorApp.calculateMansard()));
    });

  });
});