module.exports = {
    "extends": "google",
    "rules": {
        "no-invalid-this": 0,
    },
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module"
    }
};