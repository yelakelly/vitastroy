import json
from openpyxl import load_workbook

def make_compability():
	wb = load_workbook('work_1.xlsx')
	wb = wb['Совместимость']

	materials = {}

	index = 1

	for i in range(2, 44):
		cell_range = wb['A{0}:I{0}'.format(i)]
		material_id = 'material-' + str(index)

		materials[material_id] = {
			'name': cell_range[0][0].value,
		}

		material = materials[material_id]
		material['compability'] = {}

		print(cell_range[0])

		for i, val in enumerate(cell_range[0][1::], start=1):
			if val.value != 0:
				material['compability']['base-' + str(i)] = val.value

		index+=1

		print(materials)

def make_dependency_table():
	wb = load_workbook('work_1.xlsx')
	wb = wb['Коэффициенты']

	f = []

	index_range = wb['C10:CV10']
	value_range = wb['C11:CV11']

	for index, cell in enumerate(index_range[0]):
		title = index_range[0][index].value
		value = value_range[0][index].value
		f.append([title, round(value, 2)])

	print(f)

def make_price_table():
	wb = load_workbook('work_1.xlsx')
	wb = wb['Составляющие (элементы конструк']

	f = {}

	for i in range(2, 52):
		row = wb['A{0}:G{0}'.format(i)]
		_id = row[0][0].value
		_type = row[0][1].value
		title = row[0][2].value
		model = row[0][4].value
		price = row[0][5].value
		mansard = row[0][6].value

		if(not mansard):
			mansard = None
		
		if(_type == 'Фундамент'):
			val = _type + ' ' +  title.lower()
		if(_type == 'Перекрытия' or _type == 'Кровля' or _type == 'Утепление' or _type == 'Фасад' or _type == 'Инж сети'):
			val = _type + ' ' + title.lower()
		elif(title == 'Пояс монолитный'):
			continue
		elif(title == 'Утепление'):
			val = _type + ' ' + title.lower() + ' ' + model
		elif(_type == 'Отделка'):
			val = _type + ' ' + str(model)
		elif(_type.startswith('Стены')):
			val = _type + ' ' +  title.lower()
			if(model and title.startswith('Кирпич')):
				val += ' ' + str(model)
		elif(title == 'Дизайнерские'):
			val = 'Двери ' + title.lower()
		else:
			val = title
			if(model):
				val += ' ' + str(model)

		f[_id.lower()] = {
			"title": val,
			"price": (price, mansard)
		}
	print(f)
make_price_table()